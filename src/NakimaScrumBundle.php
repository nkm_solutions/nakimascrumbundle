<?php
namespace Nakima\ScrumBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaScrumBundle extends Bundle {}
