<?php
namespace Nakima\ScrumBundle\Repository;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Repository\BaseRepository;

class ProjectRepository extends BaseRepository {

    protected function transform($entity, $translator = null, $format = 'json') {
        throw new \Nakima\CoreBundle\Exception\NotYetImplementedException;
    }

    public function findAllProjects() {

        $user = $this->getUser();

        $allProjects = $this->findAll();
        if ($user->grantsRole(['ROLE_SCRUM_MANAGER'])) {
            $projects = $allProjects;
        } else {
            $projects = [];
            foreach ($allProjects as $project) {
                if ($user->getGroups()->contains('ROLE_ADMIN')) {
                    $projects[] = $project;
                } else if ($project->getProjectLeader() == $user) {
                    $projects[] = $project;
                } else if ($project->getProjectManagers()->contains($user)) {
                    $projects[] = $project;
                }
            }
        }
        return $projects;
    }
}