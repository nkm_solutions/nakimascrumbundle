<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\Utils\Number\Romaninc;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class DailyTask extends BaseEntity {

    /**
     * @ManyToOne(targetEntity="ScrumBundle\Entity\TaskStatus", cascade={"all"})
     * @JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ManyToOne(targetEntity="ScrumBundle\Entity\Task")
     * @JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $task;

    /**
     * @Column(type="integer")
     */
    protected $spent;

    /**
     * @ManyToMany(targetEntity="ScrumBundle\Entity\TaskIssue", cascade={"all"})
     * @JoinTable(name="_dailytask_taskissues"),
     *      joinColumns={@JoinColumn(name="dailytask_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="taskissue_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $issues;

    /**
     * @ManyToOne(targetEntity="ScrumBundle\Entity\Daily", inversedBy="dailyTasks")
     * @JoinColumn(name="daily_id", referencedColumnName="id")
     */
    protected $daily;

    /**
     * @Column(type="boolean")
     */
    protected $updated;

    public function __construct() {
        $this->issues = new ArrayCollection;
    }

    public function __toString() {
        return $this->getOriginalTask()->__toString();
    }

    public function setSpent($spent) {
        $this->spent = $spent;

        return $this;
    }

    public function getSpent() {
        return $this->spent;
    }

    public function setStatus(\ScrumBundle\Entity\TaskStatus $status) {
        $this->status = $status;

        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setTask(\ScrumBundle\Entity\Task $task) {
        $this->task = $task;

        return $this;
    }

    public function getTask() {
        return $this->task;
    }

    public function getUser() {
        return $this->user;
    }
    
    public function setUser(\UserBundle\Entity\User $user) {
        $this->user = $user;
        return $this;
    }

    public function addIssue(\ScrumBundle\Entity\TaskIssue $issue) {
        $this->issues[] = $issue;

        return $this;
    }

    public function removeIssue(\ScrumBundle\Entity\TaskIssue $issue) {
        $this->issues->removeElement($issue);
    }

    public function getIssues() {
        return $this->issues;
    }

    public function setDaily(\ScrumBundle\Entity\Daily $daily = null) {
        $this->daily = $daily;

        return $this;
    }

    public function getDaily() {
        return $this->daily;
    }

    public function getUpdated() {
        return $this->updated;
    }

    public function isUpdated() {
        return $this->updated;
    }
    
    public function setUpdated($updated) {
        $this->updated = $updated;
        return $this;
    }
}
