<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass(repositoryClass="Nakima\ScrumBundle\Repository\ProjectRepository")
 */
class Project extends BaseEntity {

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Column(type="string", length=4)
     */
    protected $prefix;

    /**
     * @Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @OneToOne(targetEntity="Backlog", cascade={"all"})
     * @JoinColumn(name="backlog_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $backlog;

    /**
     * @OneToMany(targetEntity="ProjectSprint", mappedBy="project", cascade={"all"})
     * @OrderBy({"number" = "ASC"})
     */
    protected $projectSprints;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\User", cascade={"all"})
     * @JoinColumn(name="projectleader_id", referencedColumnName="id")
     */
    protected $projectLeader;

    /**
     * @ManyToOne(targetEntity="ScrumBundle\Entity\ProjectStatus")
     * @JoinColumn(name="projectstatus_id", referencedColumnName="id")
     */
    protected $projectStatus;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\Group", cascade={"all"})
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ManyToMany(targetEntity="UserBundle\Entity\User", cascade={"all"})
     * @JoinTable(name="_projects_managers",
     *      joinColumns={@JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    protected $projectManagers;

    /**
     * @Column(type="integer")
     */
    protected $countTasks;

    /**
     * @OneToMany(targetEntity="ProjectTask", mappedBy="project", cascade={"all"})
     */
    protected $projectTasks;

    /**
     * @ManyToMany(targetEntity="Epic", cascade={"all"})
     * @JoinTable(name="_project_epics",
     *      joinColumns={@JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="epic_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    protected $epics;

    public function __construct() {
        $this->countTasks = 0;
        $this->createdAt = new \DateTime;
        $this->projectSprints = new ArrayCollection;
        $this->projectManagers = new ArrayCollection;
        $this->projectTasks = new ArrayCollection;

        $this->addProjectSprint(new \ScrumBundle\Entity\ProjectSprint);
    }

    public function __toString() {
        return "[$this->prefix] $this->name";
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getBacklog() {
        return $this->backlog;
    }

    public function setBacklog($backlog) {
        $this->backlog = $backlog;
        return $this;
    }

    public function getProjectStatus() {
        return $this->projectStatus;
    }

    public function setProjectStatus($projectStatus) {
        $this->projectStatus = $projectStatus;
        return $this;
    }

    public function setPrefix($prefix) {
        $this->prefix = strtoupper($prefix);

        $group = new \UserBundle\Entity\Group;
        $group->setGroup("GROUP_" . $this->prefix);

        $this->setGroup($group);

        return $this;
    }

    public function getPrefix() {
        return $this->prefix;
    }

    public function addProjectSprint($projectSprint) {
        $this->projectSprints[] = $projectSprint;

        $projectSprint->setProject($this);

        return $this;
    }

    public function removeProjectSprint($projectSprint) {
        $this->projectSprints->removeElement($projectSprint);
    }

    public function getProjectSprints() {
        return $this->projectSprints;
    }

    public function setProjectLeader($projectLeader = null) {
        $this->projectLeader = $projectLeader;

        return $this;
    }

    public function getProjectLeader() {
        return $this->projectLeader;
    }

    public function addProjectManager($projectManager) {
        $this->projectManagers[] = $projectManager;

        return $this;
    }

    public function removeProjectManager($projectManager) {
        $this->projectManagers->removeElement($projectManager);
    }

    public function getProjectManagers() {
        return $this->projectManagers;
    }

    public function setGroup($group = null) {
        $this->group = $group;

        return $this;
    }

    public function getGroup() {
        return $this->group;
    }

    public function getCountTasks() {
        return $this->countTasks;
    }

    public function setCountTasks($countTasks) {
        $this->countTasks = $countTasks;
        return $this;
    }

    public function addProjectTask($projectTask) {
        $this->projectTasks[] = $projectTask;

        return $this;
    }

    public function removeProjectTask($projectTask) {
        $this->projectTasks->removeElement($projectTask);
    }

    public function getProjectTasks() {
        return $this->projectTasks;
    }

    public function increaseCountTask() {
        $this->countTasks++;
        return $this->countTasks;
    }

    public function addEpic($epic) {
        $this->epics[] = $epic;

        return $this;
    }

    public function removeEpic($epic) {
        $this->epics->removeElement($epic);
    }

    public function getEpics() {
        return $this->epics;
    }

    public function __get($field) {
        $value = parent::__get($field);
        if ($value !== NULL) {
            return $value;
        }
        $matches = [];
        if (preg_match("/sprint-([0-9]+)/", $field, $matches)) {
            if (count($this->projectSprints) > $matches[1]) {
                return $this->projectSprints[$matches[1]];
            }
        }
    }

    public function getTotalTasks() {
        return count($this->projectTasks);
    }

    public function getTotalCompletedTasks() {
        $total = 0;
        foreach ($this->projectTasks as $projectTask) {
            if ($projectTask->getTask()->getStatus()->getName() === "VALIDATED") {
                $total++;
            }
        }

        return $total;
    }
}
