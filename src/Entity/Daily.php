<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Daily extends BaseEntity {
    
    /**
     * @OneToMany(targetEntity="ScrumBundle\Entity\DailyTask", mappedBy="daily")
     */
    protected $dailyTasks;

    /**
     * @Column(type="integer") 
     */
    protected $number;

    /**
     * @ManyToOne(targetEntity="ScrumBundle\Entity\ProjectSprint")
     * @JoinColumn(name="sprint_id", referencedColumnName="id")
     */
    protected $projectSprint;

    /**
     * @OneToOne(targetEntity="ScrumBundle\Entity\Daily", inversedBy="next")
     * @JoinColumn(name="previous_id", referencedColumnName="id")
     */
    protected $previous;

    /**
     * @OneToOne(targetEntity="ScrumBundle\Entity\Daily", mappedBy="previous")
     */
    protected $next;

    /**
     * @Column(type="datetime")
     */
    protected $createdAt;

    public function __construct() {
        $this->dailyTasks = new ArrayCollection;
        $this->createdAt = new \DateTime;
        //$this->createdAt = \DateTime::createFromFormat("U", 0);
    }

    public function getNumber() {
        return $this->number;
    }
    
    public function setNumber($number) {
        $this->number = $number;
        return $this;
    }

    public function getProjectSprint() {
        return $this->projectSprint;
    }
    
    public function setProjectSprint($projectSprint) {
        $this->projectSprint = $projectSprint;
        $projectSprint->getSprint()->addDaily($this);
        $this->setNumber(count($projectSprint->getSprint()->getDailies()));
        return $this;
    }

    public function addDailyTask(\ScrumBundle\Entity\DailyTask $dailyTask) {
        $this->dailyTasks[] = $dailyTask;
        $dailyTask->setDaily($this);

        return $this;
    }

    public function removeDailyTask(\ScrumBundle\Entity\DailyTask $dailyTask) {
        $this->dailyTasks->removeElement($dailyTask);
    }

    public function getDailyTasks() {
        return $this->dailyTasks;
    }

    public function getPrevious() {
        return $this->previous;
    }
    
    public function setPrevious($previous) {

        if ($previous) {
            $previous->setNext($this);
        }
        $this->previous = $previous;
        return $this;
    }

    public function getNext() {
        return $this->next;
    }
    
    public function setNext($next) {
        $this->next = $next;
        return $this;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUsers() {
        $users = [];
        $mapUsers = [];

        foreach ($this->dailyTasks as $dailyTask) {
            foreach ($dailyTask->getTask()->getAssignees() as $user) {
                $mapUsers[$user->getId()] = $user;
            }
        }

        foreach ($mapUsers as $user) {
            $users[] = $user;
        }

        return $users;

    }
}