<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Sprint extends BaseEntity {

    /**
     * @Column(type="integer")
     */
    protected $number;

    /**
     * @ManyToOne(targetEntity="SprintStatus")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ManyToMany(targetEntity="Daily", cascade={"all"})
     * @JoinTable(name="_sprint_dailies",
     *      joinColumns={@JoinColumn(name="sprint_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="daily_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $dailies;

    public function __construct() {
        $this->lastDaily = new \DateTime;
        $this->number = 0;
        $this->dailies = new ArrayCollection;
    }

    public function __toString() {
        return "Sprint #$this->number";
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = $number;
        return $this;
    }

    public function getMessage() {
        return "Prem el botó de crear para genera l'sprint.";
    }

    public function setMesage($message) {}

    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function addDaily($daily) {
        $this->dailies[] = $daily;
        return $this;
    }

    public function removeDaily($daily) {
        $this->dailies->removeElement($daily);
    }

    public function getDailies() {
        return $this->dailies;
    }

    public function getPrevious() {
        return $this->previous;
    }
    
    public function setPrevious($previous) {
        $this->previous = $previous;
        return $this;
    }

    public function getLastDaily() {
        return $this->lastDaily;
    }
    
    public function setLastDaily($lastDaily) {
        $this->lastDaily = $lastDaily;
        return $this;
    }
}
