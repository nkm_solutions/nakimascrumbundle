<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\Utils\Number\Romaninc;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Task extends BaseEntity {

    /**
     * @Column(type="string", length=64)
     */
    protected $title;

    /**
     * @Column(type="integer")
     */
    protected $number;

    /**
     * @Column(type="string", length=150)
     */
    protected $description;

    /**
     * @ManyToMany(targetEntity="UserBundle\Entity\User")
     * @JoinTable(name="_tasks_assignees",
     *      joinColumns={@JoinColumn(name="task_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="assignee_id", referencedColumnName="id")}
     *      )
     */
    protected $assignees;

    /**
     * @ManyToOne(targetEntity="Epic")
     * @JoinColumn(name="epic_id", referencedColumnName="id")
     */
    protected $epic;

    /**
     * @ManyToOne(targetEntity="Sprint")
     * @JoinColumn(name="sprint_id", referencedColumnName="id")
     */
    protected $sprint;

    /**
     * @ManyToOne(targetEntity="Project", cascade={"all"})
     * @JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $project;

    /**
     * @Column(type="integer")
     */
    protected $estimate;

    /**
     * @ManyToMany(targetEntity="ScrumBundle\Entity\TaskLog")
     * @JoinTable(name="_task_tasklogs",
     *      joinColumns={@JoinColumn(name="task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="tasklog_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $taskLogs;

    /**
     * @ManyToOne(targetEntity="TaskStatus")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @Column(type="integer")
     */
    protected $round;

    /**
     * @Column(type="integer")
     */
    protected $spent;

    /**
     * @Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ManyToOne(targetEntity="Task")
     * @JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $original;

    /**
     * @ManyToMany(targetEntity="UserBundle\Entity\User")
     * @JoinTable(name="_tasks_resolvers",
     *      joinColumns={@JoinColumn(name="task_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="resolver_id", referencedColumnName="id")}
     *      )
     */
    protected $resolvers;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\Group")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    public function __construct() {
        $this->datetime = new \DateTime;
        $this->number = 0;
        $this->spent = 0;
        $this->round = 1;
        $this->description = "";
        $this->assignees = new ArrayCollection;
        $this->taskLogs = new ArrayCollection;
        $this->resolvers = new ArrayCollection;
    }

    public function __toString() {
        $prefix = "???";
        if ($this->getProject()) {
            $prefix = $this->getProject()->getPrefix();
        }

        return "$prefix-$this->number | $this->title";
    }

    public function createLog($user, $time, $message) {
        $log = new \ScrumBundle\Entity\TaskLog();
        $log->setTask($this);
        $log->setSpent($time);
        if ($message !== "") {
            $issue = new \ScrumBundle\Entity\TaskIssue;
            $issue->setMessage($message);
            $issue->setAuthor($user);
            $log->setIssue($issue);
        }
        $log->setUser($user);
        $this->spent += $time;
        $this->updatedAt = new \DateTime;
        $this->addTaskLog($log);

        return $log;
    }

    public function duplicate() {
        $task = new Task();
        $task->setDescription($this->description);
        $task->setOriginal($this->getOriginal());
        $task->setRound($this->round + 1);
        $task->setTitle($this->getOriginal()->getTitle() . " (" . Romanic::toRoman($task->getRound()) . ")");
        $task->epic($this->epic);
        $task->setProject($this->getProject());
        $task->setEstimate($this->estimate);

        $this->getProject->getBacklog()->addTask($task);

        foreach ($this->assignees as $assignee) {
            $task->addAssignee($assignee);
        }

        return $task;
    }

    public function getOriginalTask() {
        if ($this->original) {
            return $this->original->getOriginal();
        }
        return $this;
    }

    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setDescription($description) {
        if (!$description)  {
            $description = "";
        }
        $this->description = $description;

        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setEstimate($estimate) {
        $this->estimate = $estimate;

        return $this;
    }

    public function getEstimate() {
        return $this->estimate;
    }

    public function setRound($round) {
        $this->round = $round;

        return $this;
    }

    public function getRound() {
        return $this->round;
    }

    public function addAssignee($assignee) {
        $this->assignees[] = $assignee;

        return $this;
    }

    public function removeAssignee($assignee) {
        $this->assignees->removeElement($assignee);
    }

    public function getAssignees() {
        return $this->assignees;
    }

    public function setEpic($epic = null) {
        $this->epic = $epic;

        return $this;
    }

    public function getEpic() {
        return $this->epic;
    }

    public function setSprint($sprint = null) {
        $this->sprint = $sprint;

        return $this;
    }

    public function getSprint() {
        return $this->sprint;
    }

    public function setProject($project = null) {
        $this->project = $project;

        $number = $project->increaseCountTask();
        $this->number = $number;

        return $this;
    }

    public function getProject() {
        return $this->project;
    }

    public function addTaskLog($taskLog) {
        $this->taskLogs[] = $taskLog;

        return $this;
    }

    public function removeTaskLog($taskLog) {
        $this->taskLogs->removeElement($taskLog);
    }

    public function getTaskLogs() {
        return $this->taskLogs;
    }

    public function setStatus($status = null) {
        $this->status = $status;
        $this->updatedAt = new \DateTime;

        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setOriginal($original = null) {
        $this->original = $original;

        return $this;
    }

    public function getOriginal() {
        return $this->original;
    }

    public function addResolver($resolver) {
        $this->resolvers[] = $resolver;

        return $this;
    }

    public function removeResolver($resolver) {
        $this->resolvers->removeElement($resolver);
    }

    public function getResolvers() {
        return $this->resolvers;
    }

    public function setGroup($group = null) {
        $this->group = $group;

        return $this;
    }

    public function getGroup() {
        return $this->group;
    }

    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setProjectTask($projectTask) {
        $this->projectTask = $projectTask;

        return $this;
    }

    public function getProjectTask() {
        return $this->projectTask;
    }

    public function initProjectTask() {
        return new ProjectTask;
    }

    public function setSpent($spent) {
        $this->spent = $spent;

        return $this;
    }

    public function getSpent() {
        return $this->spent;
    }

    public function ago() {

        $time = $this->updatedAt->format('U');

        $periods = array("segundo", "minuto", "hora", "dia", "semana", "mes", "año", "decada");
        $lengths = array("60","60","24","7","4.35","12","10");

        $now = time();

        $difference     = $now - $time;

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
           $periods[$j].= "s";
        }

        return "Hace $difference $periods[$j]";
    }

    public function getDatetime() {
        return $this->datetime;
    }
    
    public function setDatetime($datetime) {
        $this->datetime = $datetime;
        return $this;
    }

    public function removeSpent($spent) {
        $this->spent -= $spent;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }
}
