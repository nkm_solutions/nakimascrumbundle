<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class TaskLog extends BaseEntity {

    /**
     * @ManyToOne(targetEntity="Task")
     * @JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $task;

    /**
     * @Column(type="integer")
     */
    protected $spent;

    /*
     * @ManyToOne(targetEntity="TaskStatus")
     * @JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @OneToOne(targetEntity="TaskIssue", cascade={"all"})
     * @JoinColumn(name="taskissue_id", referencedColumnName="id")
     */
    protected $issue;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @Column(type="datetime")
     */
    protected $createdAt;

    public function __toString() {
        return $this->message;
    }

    public function __construct() {
        $this->createdAt = new \DateTime;
    }

    public function setTask($task) {
        $this->task = $task;

        return $this;
    }

    public function getTask() {
        return $this->task;
    }

    public function setUser($user) {
        $this->user = $user;

        return $this;
    }

    public function getUser() {
        return $this->user;
    }

    public function setSpent($spent) {
        $this->spent = $spent;

        return $this;
    }

    public function getSpent() {
        return $this->spent;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getIssue() {
        return $this->issue;
    }
    
    public function setIssue($issue) {
        $this->issue = $issue;
        return $this;
    }
}
