<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class ProjectSprint extends BaseEntity {

    /**
     * @OneToOne(targetEntity="Sprint", cascade={"all"})
     * @JoinColumn(name="sprint_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $sprint;

    /**
     * @ManyToOne(targetEntity="Project", inversedBy="projectSprints")
     * @JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * @Column(type="integer")
     */
    protected $number;

    /**
     * @OneToOne(targetEntity="ScrumBundle\Entity\ProjectSprint", inversedBy="next")
     * @JoinColumn(name="previous_id", referencedColumnName="id")
     */
    protected $previous;

    /**
     * @OneToOne(targetEntity="ScrumBundle\Entity\ProjectSprint", mappedBy="previous")
     */
    protected $next;

    public function __construct() {
        $this->sprint = new \ScrumBundle\Entity\Sprint;
    }

    public function __toString() {
        return $this->sprint->__toString();
    }

    public function getSprint() {
        return $this->sprint;
    }

    public function setSprint($sprint) {
        $this->sprint = $sprint;

        if ($this->getProject()) {
            $this->getSprint()->setNumber(count($project->getProjectSprints()));
        }
        return $this;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setNumber($number) {
        $this->number = $number;
        return $this;
    }

    public function getProject() {
        return $this->project;
    }

    public function setProject($project) {
        $this->project = $project;

        $this->setNumber(count($project->getProjectSprints()));

        if ($this->getSprint()) {
            $this->getSprint()->setNumber(count($project->getProjectSprints()));
        }

        return $this;
    }

    public function getTasks() {

        $ret = [];

        foreach ($this->project->getProjectTasks() as $projectTask) {
            if ($projectTask->getTask()->getSprint() == $this->getSprint()) {
                $ret[] = $projectTask->getTask();
            }
        }

        return $ret;
    }

    public function getPrevious() {
        return $this->previous;
    }
    
    public function setPrevious($previous) {

        if ($previous) {
            $previous->setNext($this);
        }
        $this->previous = $previous;
        return $this;
    }

    public function getNext() {
        return $this->next;
    }
    
    public function setNext($next) {
        $this->next = $next;
        return $this;
    }
}
