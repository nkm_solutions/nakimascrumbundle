<?php
namespace Nakima\ScrumBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;

use Nakima\CoreBundle\Entity\BaseEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Epic extends BaseEntity {

    /**
     * @Column(type="string", length=16)
     */
    protected $name;

    /**
     * @ManyToOne(targetEntity="EpicCategory")
     * @JoinColumn(name="epiccategory_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $epicCategory;

    public function __toString() {
        return $this->epicCategory->getName() . " / $this->name";
    }

    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setEpicCategory($epicCategory = null) {
        $this->epicCategory = $epicCategory;

        return $this;
    }

    public function getEpicCategory() {
        return $this->epicCategory;
    }
}
