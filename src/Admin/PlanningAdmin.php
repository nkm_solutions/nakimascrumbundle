<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PlanningAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('id')
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('id')
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
			->add('id')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}
}
