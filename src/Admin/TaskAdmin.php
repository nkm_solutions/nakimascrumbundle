<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;
use Nakima\ScrumBundle\Entity\ProjectTask;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class TaskAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('title')
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {

        $parameters = $this->getPersistentParameters();
        
        $em = $this->getEntityManager();
        $project = $em->getRepository("ScrumBundle:Project")->findOneById($parameters['project_id']);

        $this->getSubject()->setProject($project);
        $epicChoices = [];
        $epics = $project->getEpics();

        foreach ($epics as $epic) {
            $epicChoices[$epic->__toString()] = $epic;
        }

        $sprint = $project->getProjectSprints()->last()->getSprint();


        // ENDTODO

		$formMapper
			->add('title')
            ->add('sprint', 'choice', [
                    'choices' => [
                        'To Backlog' => null,
                        'To Sprint' => $sprint
                    ]
                ]
            )
            ->add('epic', 'choice',
                [
                    'choices' => $epicChoices
                ]
            )
            ->add('project', null, ['disabled' => true])
            ->add('estimate')
            ->add('description', null, [
                'required' => false
            ])
            ->add('assignees')
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
			->add('id')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ))
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}

    public function configureRoutes(RouteCollection $collection) {
        $collection->remove('delete');
    }

    public function prePersist($entity) {
        $em = $this->getEntityManager();

        $parameters = $this->getPersistentParameters();
        $provider = $parameters["provider"];

        if (!$entity->getSprint()) {
            $status = $em->getRepository("ScrumBundle:TaskStatus")->findOneByStatus("BACKLOG");
        } else {
            $status = $em->getRepository("ScrumBundle:TaskStatus")->findOneByStatus("TODO");
        }

        $this->getSubject()->SetStatus($status);
        $projectTask = new $provider();
        $projectTask->setProject($entity->getProject());
        $projectTask->setTask($this->getSubject());

        $em->getManager()->persist($projectTask);
    }
}
