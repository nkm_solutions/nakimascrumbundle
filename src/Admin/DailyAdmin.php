<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;
use Nakima\ScrumBundle\Form\Type\DailyType;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DailyAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
            ->add('self', DailyType::class)
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
		//	->add('id')
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
			->add('id')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}
}
