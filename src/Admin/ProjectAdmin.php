<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;
use Nakima\ScrumBundle\Form\Type\ProjectSprintType;
use Nakima\ScrumBundle\Form\Type\BacklogType;
use Nakima\ScrumBundle\Form\Type\ProjectType;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class ProjectAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
            ->add('self', ProjectType::class)
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {

        $level1 = false;

        if ($this->getSubject()->getId()) {
            $entity = $this->getSubject();

            $level1 = ($entity->getProjectLeader() == $this->getUser()) || $entity->getProjectManagers()->contains($this->getUser());
        }

        $level1 = $level1 || $this->hasRole("ROLE_ADMIN");
        $level0 = $this->hasRole("ROLE_OWNER");
        $level1 = $level1 || $level0;

        $allChoices = $this->getEntitymanager()->getRepository("UserBundle:User")->findAll();
        $roleAdmin = $this->getEntitymanager()->getRepository("UserBundle:Role")->findOneByName("ROLE_ADMIN");
        $roleAlfa = $this->getEntitymanager()->getRepository("UserBundle:Role")->findOneByName("ROLE_ALFA");
        $roleOwner = $this->getEntitymanager()->getRepository("UserBundle:Role")->findOneByName("ROLE_OWNER");

        $choices = [];

        foreach ($allChoices as $choice) {
            $a = $choice->getPermissions()->contains($roleAdmin);
            $a = $a || $choice->getPermissions()->contains($roleAlfa);
            $a = $a || $choice->getPermissions()->contains($roleOwner);

            if ($a) {
                $choices[$choice->getUsername()] = $choice;
            }
        }

		$formMapper
            ->tab("Projecte")
                ->with("Informació", ['class' => 'col-md-6'])
                    ->add('name', null, ['disabled' => !$level1])
                    ->add('prefix', null, ['disabled' => !$level1])
                ->end()
                ->with("Responsabilitats", ['class' => 'col-md-6'])
                    ->add('projectLeader', null, ['disabled' => !$level1])
                    ->add('projectManagers', 'sonata_type_model',
                        [
                            'disabled' => !$level1,
                            'btn_add' => false,
                            'multiple' => true,
                            'choices' => $choices
                        ]
                    )
                ->end()
            ->end()
		;

        $project = $this->getSubject();
        if ($this->id($project)) {
            $formMapper
                ->tab("Tasques")
                    ->with("Tasques")
                        ->add('projectTasks', 'sonata_type_collection',
                            [
                                "by_reference" => true
                            ],
                            [
                                "edit" => 'inline',
                                "inline" => 'table'
                            ]
                        )
                    ->end()
                ->end()
            ;

            $formMapper
                ->tab("Backlog")
                    ->with("Backlog")
                        ->add('self_-1', BacklogType::class)
                    ->end()
                ->end()
            ;

            foreach ($project->getProjectSprints() as $key => $sprint) {
                // is sprint status is opened
                //echo $sprint->__toString();
                $formMapper
                    ->tab($sprint->__toString())
                        ->with("Opened Tasks")
                            ->add("self_$key", ProjectSprintType::class, [
                                    'number' => $sprint->getNumber()
                                ]
                            )
                            /*->add('sprint-0.tasks', ToManyType::class, [], [ // ToOne
                                'edit' => true,
                                'add' => true,
                                'delete' => true,
                                'inline' => 'preview', // show, preview, edit
                            ])*/
                        ->end()
                    ->end()
                ;
            }
        }
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
            ->add('id')
            ->add('name')
            ->add('prefix')
            ->add('projectStatus.name')
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                ]
            ])
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}

    public function configureRoutes(RouteCollection $collection) {
        $collection->remove('delete');

        $collection
            ->add('select', 'select')
            ->add('apiPrepareDaily',  "{pid}/prepareDaily")
            ->add('apiStartTask', "{pid}/task/{tid}/start")
            ->add('apiReopenTask',  "{pid}/task/{tid}/reopen")
            ->add('apiDoneTask',  "{pid}/task/{tid}/done")
            ->add('apiValidateTask',  "{pid}/task/{tid}/validate")
            ->add('apiBacklogTask',  "{pid}/task/{tid}/backlog")
            ->add('apiLogTask',  "{pid}/task/{tid}/log")
            ->add('apiProjectReady',  "{pid}/ready")
        ;
    }

    public function prePersist($entity) {
        // TODO  PLANNING when crate
        $projectStatus = $this->getEntitymanager()->getRepository("ScrumBundle:ProjectStatus")->findOneByName("READY");

        $entity->setProjectStatus($projectStatus);
    }
}
