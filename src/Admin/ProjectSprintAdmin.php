<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProjectSprintAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('sprint')
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('sprint', 'sonata_type_model_list',
                [
                    'btn_list' => false,
                    'by_reference' => true
                ],
                [
                    'link_parameters' => [
                        "project_id" => $this->getParentFieldDescription()->getAdmin()->getSubject()->getId(),
                        "provider" => get_class($this->getSubject())
                    ]
                ]
            )
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
            ->add('sprint')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}
}
