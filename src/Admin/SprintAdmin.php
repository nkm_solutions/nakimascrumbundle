<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SprintAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('id')
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {
		$formMapper
			->add('message', 'text', ['disabled' => true])
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
			->add('id')
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}

        public function prePersist($entity) {
            $parameters = $this->getPersistentParameters();

            $em = $this->getEntityManager();
            $project = $em->getRepository("ScrumBundle:Project")->findOneById($parameters['project_id']);

            $provider = $parameters["provider"];

            $projectSprint = new $provider();
            $projectSprint->setSprint($this->getSubject());

            $project->addProjectSprint($projectSprint);

            $em->getManager()->persist($projectSprint);
        }
}
