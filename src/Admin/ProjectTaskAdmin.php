<?php
namespace Nakima\ScrumBundle\Admin;

/**
 * @author xgc1986
 */

use Nakima\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProjectTaskAdmin extends Admin {

	protected function configureShowFields(ShowMapper $showMapper) {
		$showMapper
			->add('task')
		;
	}

	protected function configureFormFields(FormMapper $formMapper) {

		$formMapper
            ->add('task', 'sonata_type_model_list',
                [
                    'btn_list' => false,
                    'by_reference' => true
                ],
                [
                    'link_parameters' => [
                        "project_id" => $this->getParentFieldDescription()->getAdmin()->getSubject()->getId(),
                        "provider" => get_class($this->getSubject())
                    ]
                ]
            )
		;
	}

	protected function configureListFields(ListMapper $listMapper) {
		unset($this->listModes['mosaic']);

		$listMapper
            ->add('task')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ))
		;
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper) {}
}
