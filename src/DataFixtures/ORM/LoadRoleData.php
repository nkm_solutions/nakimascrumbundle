<?php
namespace Nakima\ScrumBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use UserBundle\Entity\Role;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $role = new Role();
        $role->setRole('ROLE_SCRUM_ADMIN');
        $manager->persist($role);
        $this->addReference('role-scrum-admin', $role);

        $role = new Role();
        $role->setRole('ROLE_SCRUM_LEADER');
        $manager->persist($role);
        $this->addReference('role-scrum-leader', $role);

        $role = new Role();
        $role->setRole('ROLE_SCRUM_MANAGER');
        $manager->persist($role);
        $this->addReference('role-scrum-manager', $role);

        $role = new Role();
        $role->setRole('ROLE_SCRUM_DEVELOPER');
        $manager->persist($role);
        $this->addReference('role-scrum-developer', $role);

        $manager->flush();
    }

    public function getOrder() {
    	return 1;
    }
}