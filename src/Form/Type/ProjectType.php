<?php
namespace Nakima\ScrumBundle\Form\Type;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Form\AbstractBaseType;

class ProjectType extends AbstractBaseType {

    public function getDefaultOptions() {
        return [];
    }
}