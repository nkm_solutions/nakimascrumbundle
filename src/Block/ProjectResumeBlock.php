<?php
namespace Nakima\ScrumBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class ProjectResumeBlock extends \Nakima\AdminBundle\Block\AbstractResumeBlock {

	public function getName() {
		return 'project_resume_block';
	}

	function getIconClass() {
		return 'pied-piper';
	}

	function getTitle() {
		$userRepo = $this->getContainer()->get('doctrine')->getRepository('ScrumBundle:Project');
		return count($userRepo->findAll());
	}

	function getSubTitle() {
		return "Projects In Progress";
	}

	function getLinkLabel() {
		return "Create New Project";
	}

	function getColor() {
		return 'aqua';
	}

	function getDestination() {
		return '...';
	}
}