<?php

namespace Nakima\ScrumBundle\Block;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Validator\ErrorElement;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarBlock extends BaseBlockService {

    public function configureSettings(OptionsResolver $resolver) {
    
        $resolver->setDefaults([
            'template' => 'NakimaScrumBundle:Block:block_calendar.html.twig'
        ]);
    }


    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null) {

        return $this->renderResponse($blockContext->getTemplate(), [
            'block'     => $blockContext->getBlock()
         ], $response);
    }
}