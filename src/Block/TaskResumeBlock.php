<?php
namespace Nakima\ScrumBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class TaskResumeBlock extends \Nakima\AdminBundle\Block\AbstractResumeBlock {

	public function getName() {
		return 'task_resume_block';
	}

	function getIconClass() {
		return 'tasks';
	}

	function getTitle() {
		$status = $this->getContainer()
					   ->get('doctrine')
					   ->getRepository("ScrumBundle:TaskStatus")
					   ->findByStatus("VALIDATED")
		;

		$tasks = $this->getContainer()
		              ->get('doctrine')
		              ->getRepository('ScrumBundle:Task')
		              ->findByStatus($status)
        ;
		return count($tasks);
	}

	function getSubTitle() {
		return "Resolved tasks";
	}

	function getLinkLabel() {
		return "Add New Task";
	}

	function getColor() {
		return 'green';
	}

	function getDestination() {
		return '...';
	}
}