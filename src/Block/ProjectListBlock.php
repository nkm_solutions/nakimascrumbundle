<?php
namespace Nakima\ScrumBundle\Block;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class ProjectListBlock extends \Nakima\AdminBundle\Block\AbstractBlock {

	public function getTemplate() {
		return "NakimaScrumBundle:Block:project_list_block.html.twig";
	}

	public function getProjects() {
		return $this->getDoctrine()->getRepository("ScrumBundle:Project")->findAllProjects();
	}

}