<?php

namespace Nakima\ScrumBundle\Block;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Validator\ErrorElement;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectDashboardBlock extends BaseBlockService {

    protected $entityManager;
    protected $context;

    public function __construct($type, $templating, $em, $securityContext) {
        parent::__construct($type, $templating, $em, $securityContext);
        $this->em = $em;
        $this->context = $securityContext;
    }

    public function configureSettings(OptionsResolver $resolver) {
    
        $resolver->setDefaults([
            'template' => 'NakimaScrumBundle:Block:block_project_dashboard.html.twig'
        ]);
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {}

    public function execute(BlockContextInterface $blockContext, Response $response = null) {

        $projects = $this->em->getRepository("ScrumBundle:Project")->findAllProjects($this->context->getToken()->getUser());

        return $this->renderResponse($blockContext->getTemplate(), [
            'block'     => $blockContext->getBlock(),
            'projects'  => $projects
        ], $response);
    }
}