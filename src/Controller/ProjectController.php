<?php
namespace Nakima\ScrumBundle\Controller;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\AdminBundle\Controller\AdminController;
use Nakima\HipChatBundle\HipChat\HipChat;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProjectController extends AdminController {

    public function selectAction(Request $request) {

        $projects = $this->getRepo("ScrumBundle:Project")->findAllProjects($this->getUser());

        return $this->render($this->admin->getTemplate('select'), [
            'action' => 'list',
            'projects' => $projects
        ]);
    }

    public function apiPrepareDailyAction($pid) {
        // TODO check security && POST
        $this->checkMethod("POST");

        $this->checkRole("ROLE_ADMIN");

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        $this->assertTrue404($project, "Project", "id", $pid);
        $this->assertTrue401($project->getProjectStatus()->getName() === "READY");

        $projectStatus = $this->getRepo("ScrumBundle:ProjectStatus")->findOneByName("ONDAILY");

        $project->setProjectStatus($projectStatus);
        $sprint = $project->getProjectSprints()->last()->getSprint();

        $daily = $this->getRepo("ScrumBundle:Daily")->findOneBy([
            'projectSprint' => $project->getProjectSprints()->last(),
            'number' => 1
        ]);

        $mapTask = [];
        $mapDailyTask = [];
        $mapDailyTaskStatus = [];

        $tasks = $this->getRepo("ScrumBundle\Entity\Task")->findBySprint($sprint);

        foreach ($tasks as $task) {
            $mapDailyTask[$task->getId()] = $task;
            $mapTask[$task->getId()] = [
                'spent' => $task->getSpent()
            ];
        }

        $lastDaily = $daily;
        if ($daily) {
            do {
                $lastDaily = $daily;
                foreach ($daily->getDailyTasks() as $dailyTask) {
                    $mapTask[$dailyTask->getTask()->getId()]['spent'] -= $dailyTask->getSpent();
                }
            } while ($daily = $daily->getNext());
        }

        if ($lastDaily != null) {
            foreach ($lastDaily->getDailyTasks() as $dailyTask) {
                $mapDailyTaskStatus[$dailyTask->getTask()->getId()] = $dailyTask->getStatus();
            }
        }

        $em = $this->getDoctrine()->getManager();

        $daily = new \ScrumBundle\Entity\Daily;
        $daily->setProjectSprint($project->getProjectSprints()->last());
        $daily->setPrevious($lastDaily);

        foreach ($mapTask as $key => $taskInfo) {
            $dailyTask = new \ScrumBundle\Entity\DailyTask;
            $dailyTask->setTask($mapDailyTask[$key]);
            $dailyTask->setSpent($mapTask[$key]['spent']);
            $dailyTask->setStatus($mapDailyTask[$key]->getStatus());

            if ($lastDaily == null) {
                $dailyTask->setUpdated(true);
            } else {
                if (!isset($mapDailyTaskStatus[$key])) {
                    $dailyTask->setUpdated(true);
                } else {
                    $dailyTask->setUpdated($mapTask[$key]['spent'] || ($mapDailyTaskStatus[$key] == $mapDailyTask[$key]));
                }
            }

            $daily->addDailyTask($dailyTask);

            foreach ($mapDailyTask[$key]->getTaskLogs() as $taskLog) {

                if ($lastDaily) {
                    if ($taskLog->getCreatedAt() > $lastDaily->getCreatedAt()) {
                        if ($taskLog->getIssue()) {
                            $dailyTask->addIssue($taskLog->getIssue());
                        }
                    }
                } else {
                    if ($taskLog->getIssue()) {
                        $dailyTask->addIssue($taskLog->getIssue());
                    }
                }
            }

            $em->persist($dailyTask);
        }

        $em->persist($daily);

        $em->flush();

        if ($this->container->has('nakima.hipchat.transport')) {
            $hipchat = $this->get('nakima.hipchat.transport');

            $host = "http://" . $_SERVER["HTTP_HOST"] . "/scrum/daily/" . $daily->getId() . "/show";
            $message = "@all Dejar todo, el daily ya se ha creado, así que venir a $host de una puta vez.";
            $hipchat->sendNotification($message, 1449032, "green");
        }

        return new JsonResponse(
            [
                'status' => 200,
                'daily' => $daily->getId(),
                'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
            ]
        );
    }

    public function apiProjectReadyAction($pid) {

        $this->checkMethod("POST");
        $this->checkRole("ROLE_ADMIN");

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        $this->assertTrue404($project, "Project", "id", $pid);

        $projectStatus = $this->getRepo("ScrumBundle:ProjectStatus")->findOneByName("READY");

        $project->setProjectStatus($projectStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $daily = $project->getProjectSprints()->last()->getSprint()->getDailies()->last();
        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_daily.html.twig', array('object' => $daily))->getContent()
        ]);
    }

    public function startTaskAction($pid, $tid) {

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("ON_GOING");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->editAction($pid);
    }

    public function endTaskAction($pid, $tid) {

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("DONE");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->editAction($pid);
    }

    public function validateTaskAction($pid, $tid) {

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("VALIDATED");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->editAction($pid);
    }
    
    // '''api'''
    
    public function apiStartTaskAction(Request $request, $pid, $tid) {

        $this->checkMethod('POST');
        $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("ON_GOING");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }

    public function apiReopenTaskAction(Request $request, $pid, $tid) {

        $this->checkMethod('POST');
        $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("REOPENED");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }

    public function apiDoneTaskAction(Request $request, $pid, $tid) {

        $this->checkMethod('POST');
        $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("DONE");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }

    public function apiValidateTaskAction(Request $request, $pid, $tid) {

        $this->checkMethod('POST');
        $user = $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $this->assertTrue401($project->getProjectLeader() == $user || $project->getProjectManagers()->contains($user));

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("VALIDATED");

        $task->setStatus($taskStatus);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }

    public function apiBacklogTaskAction(Request $request, $pid, $tid) {

        $this->checkMethod('POST');
        $user = $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $this->assertTrue401($project->getProjectLeader() === $user || $project->getProjectManagers()->contains($user));

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);
        $taskStatus = $this->getRepo("ScrumBundle:TaskStatus")->findOneByStatus("BACKLOG");

        $task->setStatus($taskStatus);
        $task->setSprint(NULL);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }

    public function apiLogTaskAction(Request $request, $pid, $tid) {

        $time = $this->getParam("time");
        $message = $this->optParam("message", "");

        $this->checkMethod('POST');
        $user = $this->checkUser();

        $project = $this->getRepo("ScrumBundle:Project")->findOneById($pid);

        if (!$project) {
            return new \Nakima\CoreBundle\Exception\ObjectNotFoundException('Project', 'id', $pid);
        }

        if (!$this->hasRole('ROLE_ADMIN')) {
            $this->checkGroup($project->getGroup());
        }

        $this->assertTrue401($project->getProjectLeader() === $user || $project->getProjectManagers()->contains($user));

        $task = $this->getRepo("ScrumBundle:Task")->findOneById($tid);

        $taskLog = $task->createLog($this->getUser(), $time, $message);
        /*if ($message !== "") {
            $issue = new ScrumBundle\Entity\TaskIssue;
            $issue->setMessage($message);
            $issue->setAuthor($this->getUser());
            $taskLog->setIssue($issue);
        }*/

        $em = $this->getDoctrine()->getManager();
        $em->persist($taskLog);
        $em->flush();

        return new JsonResponse([
            'body' => $this->render('NakimaScrumBundle:Show:show_project.html.twig', array('object' => $project))->getContent()
        ]);
    }
}